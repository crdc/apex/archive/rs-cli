use std::process::{self, Command};
use assert_cmd::prelude::*;
use predicates::prelude::*;

#[cfg(test)]
mod cli {
    #[test]
    fn fails_without_args() -> Result<(), Box<std::error::Error>> {
        let mut cmd = Command::main_binary()?;
        cmd.arg("apex");
        cmd.assert()
            .failure()
            .stderr(predicate::str::contains("error: The following required arguments were not provided:"));

        Ok(())
    }
}
