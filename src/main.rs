//! The Apex commandline application
//!
//! This binary is used to communicate with Apex services that provide various
//! functionality for measurement, control, and analysis.

#[macro_use]
extern crate log;
extern crate env_logger;
#[macro_use]
extern crate clap;
extern crate console;
extern crate chrono;
extern crate graphql_client;
extern crate serde;
extern crate serde_yaml;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;
extern crate reqwest;

use std::env;
use std::error::Error;
use std::io;
use std::process;

use clap::{App, ArgMatches, Shell};
use console::style;

mod config;
mod types;
mod user;
mod role;
mod graphql;

use config::Config;

// TODO: use the rust recipes format for main
// TODO: something to consider https://rust-lang-nursery.github.io/cli-wg/tutorial/packaging.html
// TODO: see ripgrep for packaging https://github.com/BurntSushi/ripgrep

fn main() {
    if let Err(_) = env::var("RUST_LOG") {
        env::set_var("RUST_LOG", "apex=info,apex");
    }
    env_logger::init();

    match run() {
        Ok(_) => {}
        Err(e) => {
            error!("{}", e);
            process::exit(1);
        }
    }
}

// TODO: add a login --user --password
// TODO: implement save token to apexrc on login
// TODO: add a logout
// TODO: use token when one is present
// TODO: fix the config load workflow
// TODO: allow default config values for server
// TODO: copy config from acquire to merge env and file

fn run() -> Result<(), Box<Error>> {
    let yaml = load_yaml!("cli.yaml");
    let args = App::from_yaml(yaml)
        .args_from_usage("-v... 'Enable verbose logging'")
        .get_matches();

    handle_flags(&args);
    handle_completions(&args);

    let fname = args.value_of("config").unwrap_or("apex.yml");
    let mut config = Config::new();
    config.load(&fname).unwrap();

    // TODO: Need to check here for either config + name, or address info
    let name = args.value_of("name").unwrap_or("localhost");
    let idx = config.servers
        .iter()
        .position(|s| s.name == name)
        .unwrap();
    let server = &config.servers[idx];

    match args.subcommand() {
        ("user", Some(c)) => match c.subcommand() {
            ("read", Some(m)) => user::read(&server, m)?,
            ("list", Some(m)) => user::list(&server, m)?,
            ("add", Some(m)) => user::add(&server, m)?,
            ("edit", Some(m)) => user::edit(&server, m)?,
            ("delete", Some(m)) => user::delete(&server, m)?,
            (_, _) => unreachable!(),
        },
        ("role", Some(c)) => match c.subcommand() {
            ("read", Some(m)) => role::read(&server, m)?,
            ("list", Some(m)) => role::list(&server, m)?,
            ("add", Some(m)) => role::add(&server, m)?,
            (_, _) => unreachable!(),
        },
        ("config", Some(_)) => configuration(),
        ("experiment", Some(_)) => experiment(),
        ("vault", Some(_)) => vault(),
        ("control", Some(_)) => control(),
        ("acquisition", Some(_)) => acquisition(),
        ("analysis", Some(_)) => analysis(),
        (_, _) => unreachable!(),
    }

    Ok(())
}

fn handle_flags(args: &ArgMatches) {
    // TODO: actually do something with this
    // TODO: set env var
    match args.occurrences_of("v") {
        0 => {},
        1 => info!("{}", style("Verbose logging").green()),
        2 => info!("{}", style("Very verbose logging").blue()),
        3 => debug!("{}", style("Debug level logging").yellow()),
        4 | _ => trace!("{}", style("Trace level logging").red()),
    }
}

fn handle_completions(args: &ArgMatches) {
    let yaml = load_yaml!("cli.yaml");
    match args.subcommand() {
        ("completions", Some(c)) => {
            if let Some(shell) = c.value_of("shell") {
                App::from_yaml(yaml).gen_completions_to(
                    "apex",
                    shell.parse::<Shell>().unwrap(),
                    &mut io::stdout(),
                );
            }
            process::exit(0);
        },
        (_, _) => {},
        //(_, _) => unreachable!(),
    }
}

// FIXME: move these into a handler

fn configuration() {
    println!("{}", style("config").red());
}

fn experiment() {
    println!("experiment");
}

fn vault() {
    println!("vault");
}

fn control() {
    println!("control");
}

fn acquisition() {
    println!("acquisition");
}

fn analysis() {
    println!("analysis");
}
