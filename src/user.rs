use std::error::Error;
use clap::ArgMatches;
use reqwest::{Url};
use graphql_client::{GraphQLQuery, Response};
use std::env;

use types::*;
// XXX: would be nice to combine this in a module
use graphql::queries::{
    UserQuery, user_query,
    UsersQuery, users_query,
};
use graphql::mutations::{CreateUser, create_user};

/// Read a user from a server for a given email.
///
/// TODO: pass in settings instead of server
pub fn read(s: &Server, m: &ArgMatches) -> Result<(), Box<Error>> {
    let url: Url = format!("http://{}:{}/query", s.address, s.port)
        .parse()
        .unwrap();

    let email = value_t!(m.value_of("email"), String);

    let q = UserQuery::build_query(user_query::Variables {
        email: email.unwrap(),
    });

    let client = reqwest::Client::new();
    let mut resp = client
        .post(url)
        .bearer_auth(env::var("TOKEN")?)
        .json(&q)
        .send()?;

    let resp_body: Response<user_query::ResponseData> = resp.json()?;
    info!("{:?}", resp_body);

    if let Some(errors) = resp_body.errors {
        error!("There are errors:");
        for error in &errors {
            error!("{:?}", error);
        }
    }

    let resp_data: user_query::ResponseData = resp_body.data.expect("missing response data");

    info!("{:?}", resp_data);

    Ok(())
}

/// Read a user list from a server.
///
/// TODO: pass in settings instead of server
pub fn list(s: &Server, _m: &ArgMatches) -> Result<(), Box<Error>> {
    let url: Url = format!("http://{}:{}/query", s.address, s.port)
        .parse()
        .unwrap();

    let q = UsersQuery::build_query(users_query::Variables{});

    let client = reqwest::Client::new();
    let mut resp = client
        .post(url)
        .bearer_auth(env::var("TOKEN")?)
        .json(&q)
        .send()?;

    let resp_body: Response<users_query::ResponseData> = resp.json()?;

    if let Some(errors) = resp_body.errors {
        error!("There are errors:");
        for error in &errors {
            error!("{:?}", error);
        }
    }

    let user_data = resp_body
        .data.expect("missing response data")
        .users.edges.expect("couldn't read the user data");

    let users = user_data
        .iter()
        .flatten()
        .map(|edge| &edge.node)
        .flatten();

    for user in users {
        println!("{}", &user.email.as_ref().unwrap());
    }

    Ok(())
}

pub fn add(s: &Server, m: &ArgMatches) -> Result<(), Box<Error>> {
    let url: Url = format!("http://{}:{}/query", s.address, s.port)
        .parse()
        .unwrap();

    // TODO: check for empty/unsupplied value of variables
    let email = value_t!(m.value_of("email"), String)?;
    let password = value_t!(m.value_of("password"), String)?;

    let q = CreateUser::build_query(create_user::Variables {
        email: email,
        password: password,
    });

    let client = reqwest::Client::new();
    let mut resp = client
        .post(url)
        .bearer_auth(env::var("TOKEN")?)
        .json(&q)
        .send()?;

    let resp_body: Response<create_user::ResponseData> = resp.json()?;

    if let Some(errors) = resp_body.errors {
        error!("There are errors:");
        for error in &errors {
            error!("{:?}", error);
        }
    }

    let user_data = resp_body.data.expect("missing response data");

    info!("Created user with ID {}", user_data.create_user.unwrap().id);

    Ok(())
}

pub fn edit(_s: &Server, _m: &ArgMatches) -> Result<(), Box<Error>> {
    Ok(())
}

pub fn delete(_s: &Server, _m: &ArgMatches) -> Result<(), Box<Error>> {
    Ok(())
}
