use std::collections::HashMap;

#[derive(Debug, Serialize, Deserialize)]
pub struct Server {
    pub name: String,
    pub version: u32,
    pub address: String,
    pub port: u32
}

/// GraphQL query
///
/// This represents queries and mutations to use with GraphQL requests.
///
/// TODO: add a macro to construct a query body
#[derive(Debug, Serialize, Deserialize)]
pub struct Query {
    #[serde(rename = "query")]
    pub body: String,
    #[serde(rename = "operationName")]
    pub operation_name: String,
    pub variables: HashMap<String, String>,
}
