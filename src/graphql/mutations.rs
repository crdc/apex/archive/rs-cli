use graphql_client::GraphQLQuery;

// TODO: figure out having multiple mutations in one file

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "src/graphql/schema.json",
    query_path = "src/graphql/mutations/user.graphql",
    response_derives = "Debug",
    selected_operation = "CreateUser",
)]
pub struct CreateUser;

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "src/graphql/schema.json",
    query_path = "src/graphql/mutations/role.graphql",
    response_derives = "Debug",
    selected_operation = "CreateRole",
)]
pub struct CreateRole;
