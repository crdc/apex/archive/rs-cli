use graphql_client::GraphQLQuery;

// TODO: figure out having multiple queries in one file

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "src/graphql/schema.json",
    query_path = "src/graphql/queries/user.graphql",
    response_derives = "Debug",
    selected_operation = "User",
)]
pub struct UserQuery;

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "src/graphql/schema.json",
    query_path = "src/graphql/queries/users.graphql",
    response_derives = "Debug",
    selected_operation = "Users",
)]
pub struct UsersQuery;

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "src/graphql/schema.json",
    query_path = "src/graphql/queries/role.graphql",
    response_derives = "Debug",
    selected_operation = "Role",
)]
pub struct RoleQuery;

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "src/graphql/schema.json",
    query_path = "src/graphql/queries/roles.graphql",
    response_derives = "Debug",
    selected_operation = "Roles",
)]
pub struct RolesQuery;
