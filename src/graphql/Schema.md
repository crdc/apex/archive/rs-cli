# Schema

To generate the schema an `apex` master must be available. During development
the `apollo` CLI is what was used.

```sh
npm install -g apollo
apollo service:download --endpoint=http://apex/query > src/graphql/schema.json
```
