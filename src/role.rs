use std::error::Error;
use clap::ArgMatches;
use reqwest::{Url};
use graphql_client::{GraphQLQuery, Response};
use std::env;

use types::*;
// XXX: would be nice to combine this in a module
use graphql::queries::{
    RoleQuery, role_query,
    RolesQuery, roles_query,
};
use graphql::mutations::{CreateRole, create_role};

/// Read a role from a server for a given name.
///
/// TODO: pass in settings instead of server
pub fn read(s: &Server, m: &ArgMatches) -> Result<(), Box<Error>> {
    let url: Url = format!("http://{}:{}/query", s.address, s.port)
        .parse()
        .unwrap();

    let name = value_t!(m.value_of("name"), String);

    let q = RoleQuery::build_query(role_query::Variables {
        name: name.unwrap(),
    });

    let client = reqwest::Client::new();
    let mut resp = client
        .post(url)
        .json(&q)
        .send()?;

    let resp_body: Response<role_query::ResponseData> = resp.json()?;
    info!("{:?}", resp_body);

    if let Some(errors) = resp_body.errors {
        error!("There are errors:");
        for error in &errors {
            error!("{:?}", error);
        }
    }

    let resp_data: role_query::ResponseData = resp_body.data.expect("missing response data");

    info!("{:?}", resp_data);

    Ok(())
}

/// Read a role list from a server.
///
/// TODO: pass in settings instead of server
pub fn list(s: &Server, _m: &ArgMatches) -> Result<(), Box<Error>> {
    let url: Url = format!("http://{}:{}/query", s.address, s.port)
        .parse()
        .unwrap();

    let q = RolesQuery::build_query(roles_query::Variables{});

    let client = reqwest::Client::new();
    let mut resp = client
        .post(url)
        .bearer_auth(env::var("TOKEN")?)
        .json(&q)
        .send()?;

    let resp_body: Response<roles_query::ResponseData> = resp.json()?;

    if let Some(errors) = resp_body.errors {
        error!("There are errors:");
        for error in &errors {
            error!("{:?}", error);
        }
    }

    let role_data = resp_body
        .data.expect("missing response data")
        .roles.edges.expect("couldn't read the role data");

    let roles = role_data
        .iter()
        .flatten()
        .map(|edge| &edge.node)
        .flatten();

    for role in roles {
        println!("{}", &role.name.as_ref().unwrap());
    }

    Ok(())
}

pub fn add(s: &Server, m: &ArgMatches) -> Result<(), Box<Error>> {
    let url: Url = format!("http://{}:{}/query", s.address, s.port)
        .parse()
        .unwrap();

    // TODO: check for empty/unsupplied value of variables
    let name = value_t!(m.value_of("name"), String)?;

    let q = CreateRole::build_query(create_role::Variables {
        name: name,
    });

    let client = reqwest::Client::new();
    let mut resp = client
        .post(url)
        .bearer_auth(env::var("TOKEN")?)
        .json(&q)
        .send()?;

    let resp_body: Response<create_role::ResponseData> = resp.json()?;

    if let Some(errors) = resp_body.errors {
        error!("There are errors:");
        for error in &errors {
            error!("{:?}", error);
        }
    }

    let role_data = resp_body.data.expect("missing response data");

    info!("Created role with ID {}", role_data.create_role.unwrap().id);

    Ok(())
}
