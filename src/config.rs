use serde_yaml;
use std::fs::File;
use std::io::Read;
use std::error::Error;
use types;

#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    pub servers: Vec<types::Server>
}

impl Config {
    pub fn new() -> Config {
        Self {
            servers: Vec::new(),
        }
    }

    pub fn load(&mut self, fname: &str) -> Result<(), Box<Error>> {
        let mut file = File::open(fname)
            .expect("Unable to open the config file provided");
        let mut yaml = String::new();
        file.read_to_string(&mut yaml)
            .expect("Unable to read the config file");

        let config = serde_yaml::from_str::<Config>(&yaml);
        self.servers = config.unwrap().servers;

        Ok(())
    }
}
