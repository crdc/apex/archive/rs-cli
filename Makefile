DESTDIR = /usr
version = $(shell awk 'NR == 3 {print substr($$3, 2, length($$3)-2)}' Cargo.toml)

all:
	cargo build --release

install:
	install -Dm 755 target/release/apex "$(DESTDIR)/bin/apex"
	install -Dm 644 README.md "$(DESTDIR)/share/doc/apex/README"
	install -Dm 644 LICENSE "$(DESTDIR)/share/licenses/apex/COPYING"

uninstall:
	rm $(DESTDIR)/bin/apex

tar:
	install -Dm 755 target/release/apex apex/bin/apex
	tar cf - "apex" | xz -zf > apex_$(version)_$(shell uname -m).tar.xz
